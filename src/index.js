import React from 'react';
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button';
import './index.css';

import registerServiceWorker from './registerServiceWorker';

var createReactClass = require('create-react-class');
var ButtonUig = createReactClass({
    getInitialState: function () {
        return {value: this.props.value, status: 1}
    },
    edit: function () {
        this.setState({status: 2})
    },
    save: function () {
        this.setState({value: this.refs.textareaValue.value, status: 1})
    },
    delete: function () {
        this.props.delete(this.props.index);
    },
    renderEdit: function () {
        return (
            <div>
                <div>{this.state.value}</div>
                <Button onClick={this.edit} variant="contained" color="primary">edit</Button>
                <Button onClick={this.delete} variant="contained" color="primary">delete</Button>
            </div>
        );
    },
    renderSave: function () {
        return (
            <div>
                <textarea ref={"textareaValue"}>{this.state.value}</textarea>
                <br/>
                <Button onClick={this.save} variant="contained" color="primary">save</Button>
            </div>
        );
    },
    render: function () {
        if (this.state.status === 1) return this.renderEdit();
        else return this.renderSave()
    }
})

var Tastki = createReactClass({
    getInitialState: function () {
        return {
            tast: [
                1, 2, 3, 4
            ]
        }
    },
    delete: function (index) {
        var arr = this.state.tast;
        delete arr[index]
        this.setState({tast: arr})
    },
    addTask: function (text) {
        var arr = this.state.tast;

        arr.push(text)
        this.setState({tast: arr})
    },
    eachTask: function (item, i) {
        return (<ButtonUig key={i} index={i} value={item} delete={this.delete}/>)
    }
    ,
    render: function () {
        return (
            <div className={"Tastki"}>
                <Button onClick={this.addTask.bind(null, "new task")} variant="contained" color="primary">Add new
                    task</Button>
                {
                    this.state.tast.map(this.eachTask)
                }
            </div>
        )
    }
})
ReactDOM.render(<Tastki/>, document.getElementById('root'));
ReactDOM.render(<div className="top"> ReactJs </div>, document.getElementById('react'))
registerServiceWorker();
